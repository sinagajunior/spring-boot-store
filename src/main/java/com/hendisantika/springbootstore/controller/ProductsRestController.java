package com.hendisantika.springbootstore.controller;

import com.hendisantika.springbootstore.entity.Product;
import com.hendisantika.springbootstore.repository.ProductRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-store
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/11/18
 * Time: 07.47
 */
@RestController
@RequestMapping("/api/products")
public class ProductsRestController {

    private final ProductRepository productRepository;

    public ProductsRestController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @GetMapping
    public List<Product> getAll() {
        return productRepository.findAll();
    }

    @PostMapping
    public void create(@RequestBody Product product) {
        productRepository.saveAndFlush(product);
    }
}