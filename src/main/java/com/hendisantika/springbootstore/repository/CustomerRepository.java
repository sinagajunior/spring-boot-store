package com.hendisantika.springbootstore.repository;

import com.hendisantika.springbootstore.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-store
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/11/18
 * Time: 07.43
 */
public interface CustomerRepository extends JpaRepository<Customer, Integer> {
}
