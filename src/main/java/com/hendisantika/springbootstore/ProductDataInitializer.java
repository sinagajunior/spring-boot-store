package com.hendisantika.springbootstore;

import com.hendisantika.springbootstore.entity.Product;
import com.hendisantika.springbootstore.repository.ProductRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-store
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/11/18
 * Time: 07.49
 */
@Component
public class ProductDataInitializer implements CommandLineRunner {

    private final ProductRepository productRepository;

    public ProductDataInitializer(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public void run(String... args) {
        productRepository.save(new Product(
                "02120",
                "Pemrograman Java",
                "Pemrograman Java.",
                new BigDecimal("8.00")));

        productRepository.save(new Product(
                "01110",
                "Microservice Architecture with Spring Cloud",
                "Microservice Architecture with Spring Cloud",
                new BigDecimal(1.20)));
    }
}